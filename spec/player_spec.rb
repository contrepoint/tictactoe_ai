require 'rspec'

require_relative '../player.rb'
require_relative '../game.rb'
require_relative '../board.rb'

describe Player do

	let(:new_game) { Game.new }
	let(:new_game_board) { new_game.board }
	let(:new_player) { Player.new('x', 'human') }

  describe "#initialize" do
		it "creates a new player object" do
			expect(new_player).to be_a_kind_of(Object)
		end

		it 'saves the marker given as an attribute' do
			expect(new_player.marker).to eq('x')
		end

		it 'saves the type given as an attribute' do
			expect(new_player.type).to eq('human')
		end
	end

	describe '#pick_a_spot - valid' do
		before do
			new_player.stub(:gets => "1\n")
			new_player.pick_a_spot(new_game.board)
		end

		it 'should pick the spot if it is valid' do
			expect(new_game_board.board[1]).to eq('x')
		end
	end

	describe '#pick_a_spot - exit' do
		before do
			new_player.stub(:gets => "exit\n")
			new_player.pick_a_spot(new_game.board)
		end

		it "will exit if user types 'exit'" do
			expect { new_game_board }.raise_exception(SystemExit)
		end
	end

	describe '#pick_a_spot - invalid spot' do
		before do
			new_player.stub(:gets => "1\n")
			allow(new_game.board).to receive(:board).and_return(['o', nil, nil, nil, nil, nil, nil, nil, nil, nil])
			new_player.pick_a_spot(new_game.board)
			new_player.stub(:gets=> "2\n")
		end

		it "will not mark the spot if it is already taken" do
			expect(new_game.board.board[0]).to eq('o')
		end
	end

end
