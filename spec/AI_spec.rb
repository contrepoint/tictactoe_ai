require 'rspec'

require_relative '../player.rb'
require_relative '../AI.rb'
require_relative '../game.rb'
require_relative '../board.rb'

describe AI do

	let(:new_AI) { AI.new('x', 'AI') }
	let(:new_game) { Game.new }

  describe "#initialize" do
  	it 'inherits #initialize from Player class' do
			expect(new_AI).to be_a_kind_of(Player)
			expect(new_AI).to be_a_kind_of(AI)
		end

		it 'saves the marker given as an attribute' do
			expect(new_AI.marker).to eq('x')
		end

		it 'saves the type given as an attribute' do
			expect(new_AI.type).to eq('AI')
		end
	end

end
