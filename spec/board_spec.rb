require 'rspec'

require_relative '../board.rb'

describe Board do

	let(:new_board) { Board.new }

  describe "#initialize" do
		it "creates a new board object" do
			expect(new_board).to be_a_kind_of(Object)
		end
	end

	describe '#show' do
		it 'shows the current board' do
			expect { new_board.show }.to output( " --- --- --- \n| 0 | 1 | 2 |\n --- --- --- \n| 3 | 4 | 5 |\n --- --- --- \n| 6 | 7 | 8 |\n --- --- --- \n\n\n").to_stdout
		end
	end

end
