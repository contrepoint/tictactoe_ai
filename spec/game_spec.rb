require 'rspec'

require_relative '../player.rb'
require_relative '../AI.rb'
require_relative '../board.rb'
require_relative '../game.rb'

describe Game do

	let(:new_game) { Game.new }

  describe "#initialize" do
  	it 'initializes a new Game object' do
			expect(new_game).to be_a_kind_of(Game)
		end

		it 'initializes a new Board object' do
			expect(new_game.board).to be_a_kind_of(Board)
		end

		it 'has an empty @players array' do
			expect(new_game.players).to eq([])
		end
	end

	describe '#pick_marker' do
		before do
			new_game.stub(:gets => "x\n")
			new_game.pick_marker
		end

		it 'should create 2 new players' do
			expect(new_game.players.count).to eq(2)
		end

		it 'should have an AI player and a human player' do
			expect(new_game.players[0]).to be_a_kind_of(Player)
			expect(new_game.players[1]).to be_a_kind_of(AI)
		end

		it "the human player's marker should be the one stubbed earlier" do
			expect(new_game.players[0].marker). to eq('x')
		end
	end

	describe '#game_over when the game has just started' do
		it 'game_over? should return false' do
			expect(new_game.game_over?).to eq(false)
		end
	end

	describe '#game_over when board.won? == true' do
		before do
			allow(new_game.board).to receive(:won?).and_return(true)
			allow(new_game.board).to receive(:tie?).and_return(true)
		end

		it 'the game has been won so game_over? should return true' do
			expect(new_game.game_over?).to eq(true)
		end

		it 'the game has been tied so game_over? should return true' do
			expect(new_game.game_over?).to eq(true)
		end
	end

	# Note: Have not yet found a way to test the puts in this method.
	# expect(new_game.who_won?).to output('You won!').to_stdout gives the error
	# "expected block to output to stdout, but was not a block"
	describe '#who_won?' do
		before do
			# Create the players
			new_game.stub(:gets => "x\n")
			new_game.pick_marker

			# Set the game to be a human player win
			allow(new_game.board).to receive(:winning_marker).and_return('x')
			new_game.who_won?
		end

		it "winning marker should be human's" do
			expect(new_game.board.winning_marker).to eq(new_game.players[0].marker)
		end
	end

	describe '#who_won? - AI' do
		before do
			# Create the players
			new_game.stub(:gets => "x\n")
			new_game.pick_marker

			# Set the game to be an AI win
			allow(new_game.board).to receive(:winning_marker).and_return('o')
			new_game.who_won?
		end

		it "winning marker should be AI's" do
			expect(new_game.board.winning_marker).to eq(new_game.players[1].marker)
			expect(new_game.who_won?).to output(nil).to_stdout
		end
	end

	describe '#who_won? - AI' do
		before do
			# Create the players
			new_game.stub(:gets => "x\n")
			new_game.pick_marker

			# Set the game to be a tie
			allow(new_game.board).to receive(:winning_marker).and_return('none')
			new_game.who_won?
		end

		it "winning marker should be none" do
			expect(new_game.board.winning_marker).to eq("none")
		end
	end
end
