class Player

	attr_reader :marker, :type

	def initialize(marker, type)
		@marker = marker
		@type = type
	end

	def pick_a_spot(board)
		puts 'Where would you like to place your marker?'
		location = gets.chomp
		if %w[0 1 2 3 4 5 6 7 8].include?(location)
			if board.is_cell_empty?(location.to_i)
				board.mark_the_spot(self, location.to_i, marker)
			else
				puts 'Oops, the cell is already full!'
				pick_a_spot(board)
			end
		elsif location == 'exit'
			exit
		else
			puts "You did not select a cell between 0 and 8"
			pick_a_spot(board)
		end
	end

end