class Game

	attr_reader :board, :players

	def initialize
		@board = Board.new
		@players = []
	end

	def pick_marker
		puts "Pick x or o"
		marker = gets.chomp
		if marker == 'x'
			@players << @player = Player.new(marker, 'human')
			@players << @AI = AI.new('o', 'AI')
			puts "You picked #{@player.marker}. The computer will use #{@AI.marker}."
		elsif marker == 'o'
			@players << @player = Player.new(marker, 'human')
			@players << @AI = AI.new('x', 'AI')
			puts "You picked #{@player.marker}. The computer will use #{@AI.marker}."
		else
			puts "You did not pick a valid marker. Please choose again."
			pick_marker
		end
	end

	def micro_tutorial
		@board.show
		puts 'This is the board. When it is your turn, type the number of the cell that you want to mark with your marker.'
		puts 'To exit the game, type exit when it is your turn.'
		puts "Type 'got it' (without the quotes) to proceed."
		if gets.chomp.downcase == 'got it'
			who_starts?
		else
			micro_tutorial
		end
	end

	def who_starts?
		puts "The game keeps track of both players via an array of players called @players."
		puts "By using the shuffle method on @players, the game will randomly choose who starts first."
		puts "Let's see what happens..."
		@player1 = @players[0]
		@player2 = @players[1]
		sleep(2)
		@players.shuffle!
		@player1 = @players[0]
		@player2 = @players[1]
		puts "The shuffle has decided! The #{@player1.type} will go first."
		puts "Get ready! Starting in 3 seconds.\n3..."
		sleep(1)
		puts '2...'
		sleep(1)
		puts '1...'
		sleep(1)
		puts "0!\nGame on!"
		game_on!
	end

	def game_on!
		until game_over? == true
			@player1.pick_a_spot(@board)
			@board.show
			break if game_over? == true
			@player2.pick_a_spot(@board)
			@board.show
		end
		who_won?
	end

	def who_won?
		if @board.winning_marker == @player.marker
			puts 'You won!'
		elsif @board.winning_marker == @AI.marker
			puts 'The AI won!'
		elsif @board.winning_marker == 'none'
			puts 'It is a tie'
		end
	end

	def game_over?
		@board.won? || @board.tie?
	end

end