class AI < Player
		def pick_a_spot(board)
		if self.marker == 'x'
			opponent_marker = 'o'
		elsif self.marker == 'o'
			opponent_marker = 'x'
		end

		if will_someone_win?(board, opponent_marker) == true
		else
			if take_the_center(board) == true
			else
				if defend_opposite_corners(board, opponent_marker) == true
				else
					if defend_against_xox(board, opponent_marker) == true
					else
						if take_corner(board) == true
						else
							fill_empty_cell(board)
						end
					end
				end
			end
		end
	end

	def will_someone_win?(board, opponent_marker)
		board.lines.each_with_index do |line, index|
  		if line.select { |value| value == marker }.size == 2 && line.include?(nil)
  			coordinates = [index, line.index(nil)]
  			location = board.get_location(coordinates)
				board.mark_the_spot(self, location, marker)
  			return true
  			break
  		elsif line.select { |value| value == opponent_marker }.size == 2 && line.include?(nil)
  			coordinates = [index, line.index(nil)]
  			location = board.get_location(coordinates)
				board.mark_the_spot(self, location, marker)
  			return true
  			break
			end
		end
	end

	def take_the_center(board)
		if board.board[4] == nil
			board.mark_the_spot(self, 4, marker)
			return true
		else
			return false
		end
	end

	def defend_opposite_corners(board, opponent_marker)
		board.opposite_corner_pairs.each do |pair|
			if pair[0] == opponent_marker && pair[1] == nil
				board.mark_the_spot(self, pair[2], marker)
				return true
				break
			end
		end
	end

	def defend_against_xox(board, opponent_marker)
		b = board.board
		if b[4] == marker
			opposite_corner =[[b[0], b[8]], [b[2], b[6]]]
			line_check = [[b[1], b[7], 7], [b[3], b[5]], 5]
			opposite_corner.each do |pair|
				if pair[0] == opponent_marker && pair[1] == opponent_marker
					line_check.each do |x|
						if x[0] == nil && x[1] == nil
							board.mark_the_spot(self, x[2], marker)
							return true
							break
						end
					end
				end
			end
		end
	end

	def take_corner(board)
		board.corners.each do |corner|
			if board.is_cell_empty?(corner[1])
				board.mark_the_spot(self, corner[1], marker)
			return true
			break
			end
		end
	end

	def fill_empty_cell(board)
		board.board.each_with_index do |cell, index|
			if  board.is_cell_empty?(index)
				board.mark_the_spot(self, index, marker)
				return true
				break
			end
		end
	end

end