class Board

	attr_reader :winning_marker, :board, :b, :lines

# Initialize
  def initialize
  	@board = Array.new(9)
    @b = @board
    @winning_marker = nil
  end

# Show board
  def show
  	puts ' --- --- --- '
  	@board.each_with_index do |x, index|
  		if x == nil
  			print "| #{index} "
  		else
  			print "| #{x} "
  		end
  		if index % 3 == 2
  			puts "|"
  			puts ' --- --- --- '
  		end
  	end
		puts "\n\n"
  end

# Won?
  def won?
		if check_lines == 'x'
			@winning_marker = 'x'
			return true
		elsif check_lines == 'o'
			@winning_marker = 'o'
			return true
		else
			return false
		end
  end

  def lines
  	@lines = [ b[0..2], b[3..5], b[6..8],
  						[b[0], b[3], b[6]], [b[1], b[4], b[7]], [b[2], b[5], b[8]],
  						[b[0], b[4], b[8]], [b[2], b[4], b[6]] ]
  end

  def check_lines
  	lines.each do |x|
  		if x.uniq.size == 1 && x.uniq != [nil]
  			return x[0]
  		end
  	end
  end

# Tie?
  def tie?
  	if full?
  		@winning_marker = 'none'
  		return true
  	else
  		return false
  	end
  end

  def full?
  	return @board.all? { |a| a != nil}
  end

# Marking a spot
	def is_cell_empty?(location)
		if @board[location] == nil
			return true
		else
			return false
		end
	end

	def mark_the_spot(player, location, marker)
		@board[location] = marker
		puts "#{player.type} played #{player.marker} at location #{location}."
	end

# AI-only methods
	def get_location(coordinates)
		c = coordinates
		if [[0, 0], [3, 0], [6, 0]].include?(c)
			return 0
		elsif [[0, 1], [4, 0]].include?(c)
			return 1
		elsif [[0, 2], [5, 0], [7, 0]].include?(c)
			return 2
		elsif [[1, 0], [3, 1]].include?(c)
			return 3
		elsif [[1, 1], [4, 1], [6, 1], [7, 1]].include?(c)
			return 4
		elsif [[1, 2], [5, 1]].include?(c)
			return 5
		elsif [[2, 0], [3, 2], [7, 2]].include?(c)
			return 6
		elsif [[2, 1], [4, 2]].include?(c)
			return 7
		elsif [[2, 2], [5, 2], [6, 2]].include?(c)
			return 8
		end
	end

	def opposite_corner_pairs
  	@opposite_corner_pairs = [ [b[0], b[8], 8],
															[b[8], b[0], 0],
															[b[2], b[6], 6],
															[b[6], b[2], 2]]
  end

  def corners
  	@corners = [[b[0], 0], [b[2], 2], [b[6], 6], [b[8], 8]]
  end


end

