require_relative 'game'
require_relative 'board'
require_relative 'player'
require_relative 'AI'

game = Game.new
game.pick_marker
game.micro_tutorial